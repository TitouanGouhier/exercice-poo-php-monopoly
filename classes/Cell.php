<?php

include_once('main.php');

Class Cell extends Main
{
	public $nom;
	public $id;
	public $type;
	public $argent;
	public $proprietaire;


	public function __construct(
		Array $datas, $type, Personnage $position, $id, Personnage $argent, $prix
	) {
		$this->hydrate($datas);
		$this->taxer($type, $position, $id, $argent, $prix);
		$this->chance($type, $position, $id, $argent, $prix);

		parent::__construct();
	}

	public function hydrate(Array $datas)
	{
		$this->nom = $datas["nom"];
		$this->id = $datas["id"];
		$this->type = $datas["type"];
		$this->argent = $datas["argent"];
		$this->proprietaire = $datas["proprietaire"];
	}

	public function taxer($type, Personnage $position, $id, Personnage $argent, $prix)
	{
			if ($position == $id && $type == "taxe"){
				$argent - $prix;
			}
	}
	public function chance($type, Personnage $position, $id, Personnage $argent, $prix)
	{
			if ($position == $id && $type == "chance"){
				$argent + $prix;
			}
	}

}