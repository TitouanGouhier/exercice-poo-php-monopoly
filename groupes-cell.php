<?php

$cell1 = [
	"nom" => "Départ",
	"id" => "1",
	"type" => 'depart',
	"argent" => 1000,
	"proprietaire" => null,
];

$cell2 = [
	"nom" => "Poitier",
	"id" => "2",
	"type" => 'ville',
	"argent" => 3,
	"proprietaire" => null,
];

$cell3 = [
	"nom" => "Le mans",
	"id" => "3",
	"type" => 'ville',
	"argent" => 1000,
	"proprietaire" => null,
];

$cell4 = [
	"nom" => "chance",
	"id" => "4",
	"type" => 'chance',
	"argent" => 1000,
	"proprietaire" => null,
];

$cell5 = [
	"nom" => "Angers",
	"id" => "5",
	"type" => 'ville',
	"argent" => 12,
	"proprietaire" => null,
];

$cell6 = [
	"nom" => "Taxe",
	"id" => "6",
	"type" => 'taxe',
	"argent" => 1000,
	"proprietaire" => null,
];
$cell7 = [
	"nom" => "Dublin",
	"id" => "7",
	"type" => 'ville',
	"argent" => 750,
	"proprietaire" => null,
];