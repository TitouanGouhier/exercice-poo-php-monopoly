<?php

include_once('main.php');



/*tour de joueur 2*/
function tourJoueur_2 ($joueur_2, $joueur_1, Personnage $position, Personnage $argentPerso, Personnage $nom, Cell $argent, Cell $id, Cell $type, Cell $proprietaire)
{
/*lance le de et ce deplace*/
    $joueur_2->lancerDe(1);
/*achete une ville mais verifie sur il est bien sur une case ville qui n'a pas deja un prop*/
    if ($position != 4 || $position !=6)
    {
        if ($proprietaire != $joueur_1)
        {
            $joueur_2->acheter($id, $argentPerso, $argent, $proprietaire);
            echo $joueur_2->nom + " a acheter une ville";
        }
    }
/*il est tomber sur la case chance*/
    if ($position == 4)
    {
        $joueur_2->$argentPerso = $joueur_2->$argentPerso + 1000;
        echo $joueur_2->nom + " a eu de la chance et a gagner 1000 dollars canadien";
    }
/*il est tomber sur la case pas de chance*/
    if ($position == 6)
    {
        $joueur_2->$argentPerso = $joueur_2->$argentPerso - 1000;
        echo "Flute " + $joueur_2->nom + "n'a pas eu de chance et a perdu 1000 dollars canadien";
    }
/*passe par la case depart*/
    if ($position == 1)
    {
        $joueur_2->gagnerSalaire($argentPerso);
        echo "Enfin " + $joueur_2->nom + "a recu son salaire et a donc plus 1000 dollars canadien dans ca poche";
    }
/*a gagner la partie car son argent perso est egale ou au dessus de 7k*/
/*a l'heure actuel cela ne sert pas a grand car le jeu ce termine en 10 tours*/
    if ($joueur_2->argentPerso >= 7000)
    {
        $joueur_2->gagnerLaGame();
        echo 'OMG OMG ' + $joueur_2->nom + ' a gagner la game, ' + $joueur_1->nom + ' est probablement super nul.'; 
    }    
}


/*tour de joueur 1*/
function tourJoueur_1 ($joueur_2, $joueur_1, Personnage $position, Personnage $argentPerso, Personnage $nom, Cell $argent, Cell $id, Cell $type, Cell $proprietaire)
{
    $joueur_1->lancerDe(1);

    if ($position != 4 || $position !=6)
    {
        if ($proprietaire != $joueur_2)
        {
            $joueur_1->acheter($id, $argentPerso, $argent, $proprietaire);
            echo $joueur_1->nom + " a acheter une ville";
        }
    }
    
    if ($position == 4)
    {
        $joueur_1->$argentPerso = $joueur_1->$argentPerso + 1000;
        echo $joueur_1->nom + " a eu de la chance et a gagner 1000 dollars canadien";
    }
    
    if ($position == 6)
    {
        $joueur_1->$argentPerso = $joueur_1->$argentPerso - 1000;
        echo "Flute " + $joueur_1->nom + "n'a pas eu de chance et a perdu 1000 dollars canadien";
    }
    
    if ($position == 1)
    {
        $joueur_1->gagnerSalaire($argentPerso);
        echo "Enfin " + $joueur_1->nom + "a recu son salaire et a donc plus 1000 dollars canadien dans ca poche";
    }
    
    if ($joueur_1->argentPerso >= 7000)
    {
        $joueur_1->gagnerLaGame();
        echo 'OMG OMG ' + $joueur_1->nom + ' a gagner la game, ' + $joueur_2->nom + ' est probablement super nul.'; 
    }    
}